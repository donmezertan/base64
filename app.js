/*const str ="Ertan";
const encodeData=window.btoa(str);
const decodedData=window.atob(encodeData);
console.log(encodeData);
console.log(decodedData);*/


// All Elements
const encodeInput = document.getElementById("encode");
const decodeInput = document.getElementById("decode");
const encodeButton = document.getElementById("encodeButton");
const decodeButton = document.getElementById("decodeButton");
const decodeText = document.getElementsByClassName("decodeText");
eventListeners();

function eventListeners() {
    encodeButton.addEventListener("click", encodeData);
    decodeButton.addEventListener("click", decodeData);
}


function encodeData() {
    //Encode
    const encodeValue = encodeInput.value.trim();
    const encode = window.btoa(encodeValue);
    const encodeText = document.getElementById("encodeText");
    encodeText.textContent = "Encode Text :"+" " + encode;
    console.log(encode);
}

function decodeData() {
    //Decode
    const decodeValue = decodeInput.value.trim();
    const decode = window.atob(decodeValue);
    const decodeText = document.getElementById("decodeText");
    decodeText.textContent = "Decode Text :"+" " + decode;
    console.log(decode);
}




